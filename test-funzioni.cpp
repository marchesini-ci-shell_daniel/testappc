#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "funzioni.hpp"

TEST_CASE("SOMMA OK", "")
{
    REQUIRE(somma(0,0) == 0);
    REQUIRE(somma(2,3) == 5);
    REQUIRE(somma(2,-2) == 0);
}